package models;

public class Players {
    private String name;
    private int score;

    public Players(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public int getScore(){
        return this.score;
    }

    public void setScore(int score){
        this.score = score;
    }

}
