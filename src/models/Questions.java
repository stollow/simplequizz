package models;

public abstract class Questions {
    protected String question;
    protected String response;
    protected int difficulties;

    public Questions(String question, String response, int difficulties){
        this.question = question;
        this.response = response;
        this.difficulties = difficulties;
    }

    public String getQuestion() {
        return this.question;
    }

    public abstract void verifyAnswer(String response, Players player);

}
