package models;

public class OpenQuestions extends Questions {
    public OpenQuestions(String question, String response, int difficulties){
        super(question,response,difficulties);
    }


    public void verifyAnswer(String response, Players player) {
        if (response.toLowerCase().equals(this.response.toLowerCase())) {
            player.setScore(player.getScore() + this.difficulties);
        }
    }

}
