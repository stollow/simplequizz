package models;

public class BooleanQuestions extends Questions{

    public BooleanQuestions(String question, String response, int difficulties){
        super(question,response,difficulties);
    }

    public void verifyAnswer(String response, Players player){
        if (this.response.equals("true")){
            if (response.toLowerCase().equals("vrai")||response.toLowerCase().equals("oui")||response.toLowerCase().equals("true")) {
                player.setScore(player.getScore() + this.difficulties);
            }
        }
        if (this.response.equals("false")){
            if (response.toLowerCase().equals("faux")||response.toLowerCase().equals("non")||response.toLowerCase().equals("false")) {
                player.setScore(player.getScore() + this.difficulties);
            }
        }
    }

}
