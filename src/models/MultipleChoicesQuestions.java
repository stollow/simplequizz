package models;

public class MultipleChoicesQuestions extends Questions {

    Integer rightAnswerPosition;
    String[] answerProposition;

    public MultipleChoicesQuestions(String question, String response, int difficulties, Integer rightAnswerPosition,String[] answerProposition){
        super(question,response,difficulties);
        this.rightAnswerPosition = rightAnswerPosition;
        this.answerProposition = answerProposition;
    }

    public String getQuestion(){
        String question =super.getQuestion();
        String choices="";
        for(String choice : answerProposition){
            choices += choice + System.lineSeparator();
        }
        return question + System.lineSeparator() + choices;
    }

    public void verifyAnswer(String response, Players player){
        if (response.toLowerCase().equals(this.response.toLowerCase()) || response.equals(rightAnswerPosition.toString())) {
            player.setScore(player.getScore() + this.difficulties);
        }
    }

}
