package tests;

import models.*;
import org.junit.jupiter.api.*;

public class BooleanQuestionsTest {
    Questions question;
    Players player;

    @BeforeEach
    void init() {

        this.question = new BooleanQuestions("L'humain d'idéfix s'appelle Obélix ? Vrai ou faux ?", "true", 1);
        this.player = new Players("charles");
    }

    @Test
    @DisplayName("Questions access tests")
    void getBooleanQuestionsTest(){
        Assertions.assertAll(
                // test BooleanQuestions
                ()->Assertions.assertEquals(question.getQuestion(),"L'humain d'idéfix s'appelle Obélix ? Vrai ou faux ?"),
                ()->Assertions.assertNotEquals(question.getQuestion(),"Cette question est elle débile ?"));
    }

    @Test
    void verifyAnswerTest(){
        question.verifyAnswer("true",player);
        Assertions.assertEquals(1,player.getScore());
        player.setScore(0);
        question.verifyAnswer("oui",player);
        Assertions.assertEquals(1,player.getScore());
        player.setScore(0);
        question.verifyAnswer("vrai",player);
        Assertions.assertEquals(1,player.getScore());
        player.setScore(0);

        question.verifyAnswer("false",player);
        Assertions.assertNotEquals(1,player.getScore());
        player.setScore(0);
        question.verifyAnswer("non",player);
        Assertions.assertNotEquals(1,player.getScore());
        player.setScore(0);
        question.verifyAnswer("faux",player);
        Assertions.assertNotEquals(1,player.getScore());
        player.setScore(0);
    }
}
