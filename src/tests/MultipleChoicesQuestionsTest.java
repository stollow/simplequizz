package tests;

import models.BooleanQuestions;
import models.MultipleChoicesQuestions;
import models.Players;
import models.Questions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MultipleChoicesQuestionsTest {
    Questions question;
    Players player;

    @BeforeEach
    void init() {
        String[] answerProposition = {"Milou", "Bernard", "Jacques"};
        this.question= new MultipleChoicesQuestions("Comment s'appelle le chien de Tintin ?", "Milou", 3, 1, answerProposition);
        this.player = new Players("charles");
    }
    @Test
    @DisplayName("Questions access tests")
    void getBooleanQuestionsTest(){
        Assertions.assertAll(
                // test MultipleChoicesQuestions
                ()->Assertions.assertEquals(question.getQuestion(),"Comment s'appelle le chien de Tintin ?"+System.lineSeparator()+"Milou"+System.lineSeparator()+"Bernard"+System.lineSeparator()+"Jacques"+System.lineSeparator()),
                ()->Assertions.assertNotEquals(question.getQuestion(),"Cette question est elle débile ?"));

    }

    @Test
    void verifyAnswerTest(){
        question.verifyAnswer("1",player);
        Assertions.assertEquals(3,player.getScore());
        player.setScore(0);
        question.verifyAnswer("Milou",player);
        Assertions.assertEquals(3,player.getScore());
        player.setScore(0);
        question.verifyAnswer("milou",player);
        Assertions.assertEquals(3,player.getScore());
        player.setScore(0);
        question.verifyAnswer("2",player);
        Assertions.assertNotEquals(3,player.getScore());
        player.setScore(0);
        question.verifyAnswer("Bertrand",player);
        Assertions.assertNotEquals(3,player.getScore());
        player.setScore(0);

    }
}
