package tests;

import models.OpenQuestions;
import models.Players;
import models.Questions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class OpenQuestionsTest {
    Questions question;
    Players player;

    @BeforeEach
    void init() {

        this.question = new OpenQuestions("Quel est la couleur du cheval blanc d'Henri IV ?", "Blanc", 2);
        this.player = new Players("charles");
    }

    @Test
    @DisplayName("Questions access tests")
    void getBooleanQuestionsTest(){
        Assertions.assertAll(

                // test OpenQuestions
                ()->Assertions.assertEquals(question.getQuestion(),"Quel est la couleur du cheval blanc d'Henri IV ?"),
                ()->Assertions.assertNotEquals(question.getQuestion(),"Cette question est elle débile ?"));
    }

    @Test
    void verifyAnswerTest(){
        question.verifyAnswer("Blanc",player);
        Assertions.assertEquals(2,player.getScore());
        player.setScore(0);
        question.verifyAnswer("Noir",player);
        Assertions.assertNotEquals(2,player.getScore());
        player.setScore(0);
        question.verifyAnswer("blanc",player);
        Assertions.assertEquals(2,player.getScore());
        player.setScore(0);
    }
}
