import filters.SortByScore;
import models.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SimpleQuiz {
    public static void main(String[] args) {
        int nbPlayer = 0;
        String[] answerProposition = {"Milou","Bernard","Jacques"};
        Questions question1 = new OpenQuestions("Quel est la couleur du cheval blanc d'Henri IV ?","Blanc",2);
        Questions question2 = new MultipleChoicesQuestions("Comment s'appelle le chien de Tintin ?","Milou",3,1, answerProposition);
        Questions question3 = new BooleanQuestions("L'humain d'idéfix s'appelle Obélix ? Vrai ou faux ?","true",1 );

        ArrayList<Questions> questions = new ArrayList<>();
        ArrayList<Players> listPlayers = new ArrayList<>();

        questions.add(question1);
        questions.add(question2);
        questions.add(question3);

        Scanner sc = new Scanner(System.in);
        System.out.println("Quel est le nombre de joueur ?");

        try
        {
            nbPlayer = sc.nextInt();
            if(nbPlayer==0){
                System.out.println("Invalid Input");
            }
        }
        catch (java.util.InputMismatchException e)
        {
            System.out.println("Invalid Input");
            return;
        }

        for(int i = 1; i<=nbPlayer;i++) {
            Scanner sc2 = new Scanner(System.in);
            System.out.println("Joueur "+i+" Quel est votre nom ?");
            String name = sc2.nextLine();
            Players player = new Players(name);
            listPlayers.add(player);
        }
        for(int j = 0; j<listPlayers.size();j++) {
            for (int i = 0; i < questions.size(); i++) {
                Scanner sc3 = new Scanner(System.in);
                System.out.println(listPlayers.get(j).getName()+": "+questions.get(i).getQuestion());
                String response = sc3.nextLine();
                questions.get(i).verifyAnswer(response,listPlayers.get(j));
            }

            if (listPlayers.get(j).getScore() == 0) {
                System.out.println("Ca n'est pas fameux, " + listPlayers.get(j).getName() + ", votre score est 0...");
            } else {
                System.out.println("Bravo, " + listPlayers.get(j).getName() + "! Votre score est " + listPlayers.get(j).getScore() + ".");
            }
        }

        Collections.sort(listPlayers, new SortByScore());
        System.out.println("Classement: ");
        for(int j = 0; j<listPlayers.size();j++){
            System.out.println(listPlayers.get(j).getName() + " Votre score est: " + listPlayers.get(j).getScore());
        }
    }
}
