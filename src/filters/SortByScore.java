package filters;

import models.Players;

import java.util.Comparator;

public class SortByScore implements Comparator<Players> {

    public int compare(Players a, Players b){
        return b.getScore() - a.getScore();
    }
}
